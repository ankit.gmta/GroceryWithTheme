import Layout from '@components/layout/layout';
import AccountLayout from '@components/my-account/account-layout';
import OrderDetails from '@components/order/order-details';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
// import { GetServerSideProps } from 'next';
import { GetStaticProps, GetStaticPaths  } from 'next';
import bundles from '../../../../data/bundles.json';

export default function OrderPage() {
  return (
    <AccountLayout>
      <OrderDetails className="p-0" />
    </AccountLayout>
  );
}

OrderPage.Layout = Layout;

export const getStaticProps: GetStaticProps = async ({
  locale,
}: any) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'common',
        'forms',
        'menu',
        'footer',
      ])),
    },
  };
};
export const getStaticPaths: GetStaticPaths = async () => {
  // Fetch the order IDs from your API or data source
  const orderIds = [1, 2, 3];

  // Generate the array of paths based on the order IDs
  const paths = orderIds.map((id) => ({
    params: { id: id.toString() }, // Ensure the ID is passed as a string
  }));

  return {
    paths,
    fallback: true,
  };
};
