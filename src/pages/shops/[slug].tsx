import Layout from '@components/layout/layout';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
// import { GetServerSideProps } from 'next';
import { GetStaticProps, GetStaticPaths  } from 'next';
import bundles from '../../../data/bundles.json';
import ShopsSingleDetails from '@components/shops/shops-single-details';
import DownloadApps from '@components/common/download-apps';

export default function ShopDetailsPage() {
  return (
    <>
      <ShopsSingleDetails />
      <DownloadApps />
    </>
  );
}

ShopDetailsPage.Layout = Layout;

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale!, [
        'common',
        'forms',
        'menu',
        'footer',
      ])),
    },
  };
};
// Generate an array of slugs based on the `bundles` data
export const getStaticPaths: GetStaticPaths = async () => {
  const slugs = bundles.map((bundle: { slug: any; }) => ({
    params: { slug: bundle.slug },
  }));

  return {
    paths: slugs,
    fallback: false,
  };
};
