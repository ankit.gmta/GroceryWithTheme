export const languages = ['en']

export const defaultLanguage = 'en'

// Locale files under /src/locales/[lang]/
export const namespaces = ['common', 'meta', 'error']

export const defaultNamespace = 'common'