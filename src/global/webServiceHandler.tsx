import { useState, useEffect } from "react";
// export  const MainGlobalURL = 'http://127.0.0.1:8000';
// export const MainbaseURL = MainGlobalURL+'/api/';

export const URLS = {
    MainGlobalURL: 'http://118.189.213.8:8053/',
    MainbaseURL: 'http://118.189.213.8:8053/api/',
    ImageURL : '/assets/images/',
    // Add more API URLs as needed
  };


export async function ApiCall(url = '', newData = {}){
    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(newData)
    });
    return response.json();
}
